import { textColor } from '@/lib/text-color'

export const logger = {
  success: (message: string) => {
    console.log(textColor.green(message))
  },
  warning: (message: string) => {
    console.warn(textColor.yellow(message))
  },
  error: (message: string) => {
    console.error(textColor.red(message))
  },
  break: () => {
    console.log('')
  }
}
