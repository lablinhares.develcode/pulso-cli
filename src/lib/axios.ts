import _axios, { type CreateAxiosDefaults } from 'axios'

export function axios(options: CreateAxiosDefaults) {
  return _axios.create(options)
}
