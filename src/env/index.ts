import { z } from 'zod'

import { logger } from '@/utils/logger'

const envSchema = z.object({
  FIGMA_ACCESS_TOKEN: z.string()

})

const _env = envSchema.safeParse(process.env)

if (!_env.success) {
  const problemEnvironmentVariables = Object.keys(_env.error.formErrors.fieldErrors)
    .join(',')

  const errorReason = `Your .env file will need to have the following environment variables: ${problemEnvironmentVariables}`

  logger.error(errorReason)
  process.exit(1)
}

export const env = _env.data
